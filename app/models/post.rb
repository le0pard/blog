class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy

  validates :title,
    presence: true,
    length: { maximum: 250 }

  validates :body,
    presence: true,
    length: { maximum: 5000 }
end
